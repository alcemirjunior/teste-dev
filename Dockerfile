FROM openjdk:11
VOLUME /tmp
EXPOSE 8080
ADD ./target/teste-dev-pleno-1.0.0-SNAPSHOT-runner.jar teste-dev-pleno.jar
ENTRYPOINT ["java","-jar","/teste-dev-pleno.jar"]
